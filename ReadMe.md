# Smart battery device analyzer

This tool generates a lot of information by just plugging in 3 pins in yor battery (SDL, SDC, GND). See some pictures below and the other resources.

## Disclaimer
I don't know (yet) any unseal, full access or power failure clear key. I am also looking for them, please contact me, if you have any of them.


## Example data
```

Reading data...

** Static data **
  Manufacturer name: LGC 11
  Chemistry: LION
  Device name: LNV-45N1153

** Dynamic data **
 Remaining capacity alarm: 994 min
 Remaining time alarm: 10 min
 Battery mode: 0b1110000000100100
  - Internal charger: 0
  - Primary battery support: 0
  - Condition flag: Battery OK
  - Charge controller: 0
  - Primary battery: 0
  - Alarm mode: 0
  - Charger mode: 1
  - Capacity mode: mA / mAh
 At rate: 0mA / 10 mW
 At rate time to full: 65535min
 At rate time to empty: 65535min
 At rate ok: 0
 Temperature: 25.15 °C
 Voltage: 11889mV
 Current: 0mA
 Average current: 0mA
 Cell 4 voltage: 19472mV
 Cell 3 voltage: 0mV
 Cell 2 voltage: 12033mV
 Cell 1 voltage: 18326mV

** Manufacturer data **
 Device type: 0x1002
 Firmware version: 10.2
 Hardware version: 0x1002
 DF checksum: 0x1002
 Manufacturer status: 0b1000000000010
 FET status: Charge on, discharge on
 Permanent failure status: Cell imbalance failure
 Battery state: Wake up
 Chemistry id: 0x1002
```
(This battery is broken and the permanent failure flag is set).


## Hardware used
 - Arduino UNO (all others should also work, but you probably have to change the pins)
 - 3 Wires
 - 2x 10k Resistors (for pulling up SDL and SDC)

## Example setup
(Remember: I only have 1 Lenovo Thinkpad W540)

![](doc/IMG_20210518_203155.jpg)

Pin map:
 - T: Temperature
 - SDL: I2C Data line
 - SDC: I2C Clock line.
 
![](doc/IMG_20210517_215259.jpg)

![](doc/IMG_20210517_215309.jpg)

![](doc/IMG_20210517_215421.jpg)



## My story

I've been looking on a battery for my Thinkpad W540. I was to stingy to buy a new one, and after a I while a [friend on mine](https://sj4jc.de) "donated" me one (I gave him chocolate in return).
Also another "pro requirement" was, that the battery is almost dead (So I could recell it). I have a couple of expensive Lithium-ion 18650 cells "laying around", that I use for most of my hardware stuff.
I already read online, that I should not disconnect the old cells, because a fuse would blow... Yah. Guess what happened :P. Just 1 bad solder joint... I quickly found the fuse (labeled as `F1` on the pcb).
After bridging it, the battery still did not function. I started googeling and found out, that the chip commited suicide. The permanent failure flag was set.

 
## Removing the failure flag
(For more details look into [doc/sluu276.pdf](doc/sluu276.pdf))  
The battery has 4 access modes:
 - Sealed: just reading data, setting charge limits, basically nothing
 - Unsealed: pretty much anything. Chaning static data, like target voltage, capacity, ...
 - Full access: dumping data (like ram?)
 - Boot rom: Dumping firmware, ...
 
To go into Boot rom, we need to do the following:
 1. Go into unsealed mode (Sending the 4 byte unseal key)
 2. Go into full access mode (Sending the 4 byte full access key)
 3. Send the boot rom command
 
To remove the failure flag, we need another key, called `PFKey`. So first go into unsealed mode, then clear the flag with the pf key.


## Resources, Links
 - http://www.karosium.com/2016/08/smbusb-hacking-smart-batteries.html
 - https://github.com/laszlodaniel/SmartBatteryHack
 - http://zmatt.net/unlocking-my-lenovo-laptop-part-1/