#include <Arduino.h>
#include <main.h>


uint8_t buffer[MAX_BUFFER_SIZE];

int i2c_smbus_read_word(uint8_t command) {
    i2c_start((DEFAULT_DEVICE_ADDRESS << 1) | I2C_WRITE);
    i2c_write(command);
    i2c_rep_start((DEFAULT_DEVICE_ADDRESS << 1) | I2C_READ);
    byte data1 = i2c_read(false);
    byte data2 = i2c_read(true);
    i2c_stop();
    return ((int) data1) | (((int) data2) << 8);
}

void i2c_smbus_read_data(uint8_t command, uint8_t size, uint8_t *data) {
    i2c_start((DEFAULT_DEVICE_ADDRESS << 1) + I2C_WRITE);
    i2c_write(command);
    i2c_rep_start((DEFAULT_DEVICE_ADDRESS << 1) + I2C_READ);
    for (int i = 0; i < size - 1; i++) {
        data[i] = i2c_read(false);
    }
    data[size - 1] = i2c_read(true);
    i2c_stop();
}

void i2c_smbus_write_data(uint8_t command, uint8_t size, uint8_t *data) {
    i2c_start((DEFAULT_DEVICE_ADDRESS << 1) + I2C_WRITE);
    i2c_write(command);
    for (int i = 0; i < size; i++) {
        i2c_write(data[i]);
    }
    i2c_stop();
}

void i2c_smbus_write_word(uint8_t command, uint16_t data) {
    i2c_start((DEFAULT_DEVICE_ADDRESS << 1) + I2C_WRITE);
    i2c_write(command);
    i2c_write(data & 0xFF);
    i2c_write((data >> 8) & 0xFF);
    i2c_stop();
}

int read_manufacturer_data(uint16_t command) {
    i2c_smbus_write_word(COMMAND_MANUFACTURER_ACCESS, command);
    return i2c_smbus_read_word(COMMAND_MANUFACTURER_ACCESS);
}

uint8_t i2c_smbus_read_block(uint8_t command, uint8_t *block_buffer) {
    i2c_start((DEFAULT_DEVICE_ADDRESS << 1) + I2C_WRITE);
    i2c_write(command);
    i2c_rep_start((DEFAULT_DEVICE_ADDRESS << 1) + I2C_READ);
    uint8_t data_length = i2c_read(false);
    data_length = constrain(data_length, 0, MAX_BUFFER_SIZE - 1);
    for (uint8_t i = 0; i < data_length - 1; i++) {
        block_buffer[i] = i2c_read(false);
    }
    block_buffer[data_length - 1] = i2c_read(true);
    i2c_stop();
    return data_length;
}

void print_manufacturer_data(const char prefix[], uint8_t command) {
    uint16_t data_read = read_manufacturer_data(command);
    Serial.print(" ");
    Serial.print(prefix);
    Serial.println(data_read, HEX);
}

void print_manufacturer_data() {
    Serial.println();
    Serial.println("** Manufacturer data **");

    print_manufacturer_data("Device type: 0x", MANUFACTURER_ACCESS_DEVICE_TYPE);

    uint16_t firmware_version = read_manufacturer_data(MANUFACTURER_ACCESS_FIRMWARE_VERSION);
    Serial.print(" Firmware version: ");
    Serial.print((firmware_version >> 8) & 0xFF, HEX);
    Serial.print(".");
    Serial.println((firmware_version) & 0xFF, HEX);

    print_manufacturer_data("Hardware version: 0x", MANUFACTURER_ACCESS_HARDWARE_VERSION);


    i2c_smbus_write_word(COMMAND_MANUFACTURER_ACCESS, MANUFACTURER_ACCESS_DF_CHECKSUM);
    delay(45); // The response takes up to 45 ms
    uint16_t checksum = i2c_smbus_read_word(COMMAND_MANUFACTURER_ACCESS);

    Serial.print(" DF checksum: 0x");
    Serial.println(checksum, HEX);

    uint16_t manufacturer_status = read_manufacturer_data(MANUFACTURER_ACCESS_MANUFACTURER_STATUS);
    Serial.print(" Manufacturer status: 0b");
    Serial.println(manufacturer_status, BIN);

    Serial.print(" FET status: ");
    uint16_t fet_status = (manufacturer_status >> 14) & 0b11;
    switch (fet_status) {
        case 0b00:
            Serial.println("Charge on, discharge on");
            break;
        case 0b01:
            Serial.println("Charge off, discharge on");
            break;
        case 0b10:
            Serial.println("Charge off, discharge off");
            break;
        case 0b11:
            Serial.println("Charge on, discharge off");
            break;
        default:
            break;
    }

    Serial.print(" Permanent failure status: ");
    uint16_t failure_status = (manufacturer_status >> 12) & 0b11;
    switch (failure_status) {
        case 0b00:
            Serial.println("Fuse blown / nothing");
            break;
        case 0b01:
            Serial.println("Cell imbalance failure");
            break;
        case 0b10:
            Serial.println("Safety voltage failure");
            break;
        case 0b11:
            Serial.println("FET failure");
            break;
        default:
            break;
    }

    Serial.print(" Battery state: ");
    uint16_t battery_state = (manufacturer_status >> 8) & 0b1111;
    switch (battery_state) {
        case 0b0000:
            Serial.println("Wake up");
            break;
        case 0b0001:
            Serial.println("Normal discharge");
            break;
        case 0b0011:
            Serial.println("Pre charge");
            break;
        case 0b0101:
            Serial.println("Charge");
            break;
        case 0b0111:
            Serial.println("Charge termination");
            break;
        case 0b1000:
            Serial.println("Fault charge terminate");
            break;
        case 0b1001:
            Serial.println("Permanent failure");
            break;
        case 0b1010:
            Serial.println("Over current");
            break;
        case 0b1011:
            Serial.println("Over temperature");
            break;
        case 0b1100:
            Serial.println("Battery failure");
            break;
        case 0b1110:
            Serial.println("Reserved");
            break;
        case 0b1111:
            Serial.println("Battery removed");
            break;
        default:
            break;
    }

    print_manufacturer_data("Chemistry id: 0x", MANUFACTURER_ACCESS_CHEMISTRY_ID);
}


void print_block(const char prefix[], const char suffix[], uint8_t command) {
    uint8_t bytes_read = i2c_smbus_read_block(command, buffer);
    Serial.print(" ");
    Serial.print(prefix);
    Serial.write(buffer, bytes_read);
    Serial.print(suffix);
    Serial.println();
}

void print_block(const char prefix[], uint8_t command) {
    print_block(prefix, "", command);
}

void print_word(const char prefix[], const char suffix[], uint8_t command, uint8_t encoding) {
    uint16_t data = i2c_smbus_read_word(command);
    Serial.print(" ");
    Serial.print(prefix);
    Serial.print(data, encoding);
    Serial.print(suffix);
    Serial.println();
}


void print_word(const char prefix[], uint8_t command, uint8_t encoding) {
    print_word(prefix, "", command, encoding);
}

bool is_mask(uint16_t data, uint16_t mask) {
    return (data & mask) == mask;
}

void print_static_info() {
    Serial.println();
    Serial.println("** Static data **");
    print_block(" Manufacturer name: ", COMMAND_MANUFACTURER_NAME);
    print_block(" Chemistry: ", COMMAND_DEVICE_CHEMISTRY);
    print_block(" Device name: ", COMMAND_DEVICE_NAME);
}

void print_dynamic_data() {
    Serial.println();
    Serial.println("** Dynamic data **");

    print_word("Remaining capacity alarm: ", " min", COMMAND_REMAINING_CAPACITY_ALARM, DEC);
    print_word("Remaining time alarm: ", " min", COMMAND_REMAINING_TIME_ALARM, DEC);

    uint16_t battery_mode = i2c_smbus_read_word(COMMAND_BATTERY_MODE);

    Serial.print(" Battery mode: 0b");
    Serial.println(battery_mode, BIN);


    Serial.print("  - Internal charger: ");
    Serial.println(is_mask(battery_mode, BATTERY_MODE_ICC_MASK));

    Serial.print("  - Primary battery support: ");
    Serial.println(is_mask(battery_mode, BATTERY_MODE_PRIMARY_BATTERY_SUPPORT));

    Serial.print("  - Condition flag: ");
    if (is_mask(battery_mode, BATTERY_MODE_CONDITION_FLAG)) {
        Serial.println("Condition cycle requested");
    } else {
        Serial.println("Battery OK");
    }

    Serial.print("  - Charge controller: ");
    Serial.println(is_mask(battery_mode, BATTERY_MODE_CHARGE_CONTROLLER));

    Serial.print("  - Primary battery: ");
    Serial.println(is_mask(battery_mode, BATTERY_MODE_PRIMARY_BATTERY));

    Serial.print("  - Alarm mode: ");
    Serial.println(is_mask(battery_mode, BATTERY_MODE_ALARM_MODE));

    Serial.print("  - Charger mode: ");
    Serial.println(is_mask(battery_mode, BATTERY_MODE_CHARGE_MODE));

    Serial.print("  - Capacity mode: ");
    if (is_mask(battery_mode, BATTERY_MODE_CAPACITY_MODE)) {
        Serial.println("mA / mAh");
    } else {
        Serial.println("10 mW / mWh");
    }


    print_word("At rate: ", "mA / 10 mW", COMMAND_AT_RATE, DEC); // ToDo: Signed integer
    print_word("At rate time to full: ", "min", COMMAND_AT_RATE_TO_FULL, DEC);
    print_word("At rate time to empty: ", "min", COMMAND_AT_RATE_TIME_TO_EMPTY, DEC);

    uint16_t at_rate_ok = i2c_smbus_read_word(COMMAND_AT_RATE_OK);
    Serial.print(" At rate ok: ");
    Serial.println(at_rate_ok == 0);

    float temperature = ((float) i2c_smbus_read_word(COMMAND_TEMPERATURE) * 0.1f) - 273.15f;
    Serial.print(" Temperature: ");
    Serial.print(temperature);
    Serial.println(" °C");

    print_word("Voltage: ", "mV", COMMAND_VOLTAGE, DEC);
    print_word("Current: ", "mA", COMMAND_CURRENT, DEC); // ToDo: Signed integer
    print_word("Average current: ", "mA", COMMAND_AVERAGE_CURRENT, DEC); // ToDo: Signed integer

    print_word("Cell 4 voltage: ", "mV", COMMAND_CELL_4_VOLTAGE, DEC);
    print_word("Cell 3 voltage: ", "mV", COMMAND_CELL_3_VOLTAGE, DEC);
    print_word("Cell 2 voltage: ", "mV", COMMAND_CELL_2_VOLTAGE, DEC);
    print_word("Cell 1 voltage: ", "mV", COMMAND_CELL_1_VOLTAGE, DEC);
}


void test_unsealed() {
    uint16_t design_capacity = i2c_smbus_read_word(COMMAND_DESIGN_CAPACITY);
    i2c_smbus_write_word(COMMAND_DESIGN_CAPACITY, 12345);
    uint16_t new_design_capacity = i2c_smbus_read_word(COMMAND_DESIGN_CAPACITY);
    if (new_design_capacity == 12345) {
        Serial.println("Device is in unsealed / full access mode. Congratulation!");
        i2c_smbus_write_word(COMMAND_DESIGN_CAPACITY, design_capacity);
    } else {
        Serial.println("Could not unseal device! :(");
    }
}

void print_pf_status() {
    i2c_smbus_write_word(COMMAND_MANUFACTURER_ACCESS, COMMAND_PF_STATUS);
    uint16_t pf_status = i2c_smbus_read_word(COMMAND_MANUFACTURER_ACCESS);

    Serial.print("Operation status: 0b");
    Serial.println(pf_status, BIN);
    Serial.print("  - Fuse blown: ");
    Serial.println(is_mask(pf_status, PF_STATUS_FUSE_BLOWN_BIT));
    Serial.print("  - Pf shut: ");
    Serial.println(is_mask(pf_status, PF_STATUS_PF_SHUT_BIT));

}

void print_operation_status() {
    i2c_smbus_write_word(COMMAND_MANUFACTURER_ACCESS, COMMAND_OPERATION_STATUS);
    uint16_t operation_status = i2c_smbus_read_word(COMMAND_MANUFACTURER_ACCESS);

    Serial.print("Operation status: 0b");
    Serial.println(operation_status, BIN);
    Serial.print("  - Access mode: ");
    if (is_mask(operation_status, OPERATION_STATUS_SEALED_BIT)) {
        Serial.println("Sealed");
    } else {
        Serial.println("Unsealed");
    }
    Serial.print("  - Full access mode: ");
    if (is_mask(operation_status, OPERATION_STATUS_FULL_ACCESS_MODE_BIT)) {
        Serial.println("Disabled");
    } else {
        Serial.println("Enabled");
    }
}


void unseal(uint32_t key) {
    Serial.println("Waiting for unseal...");
    delay(5000);
    Serial.print("Trying to unseal with key 0x");
    Serial.println(key, HEX);
    i2c_smbus_write_word(COMMAND_MANUFACTURER_ACCESS, (key >> 16) & 0xFFFF);
    i2c_smbus_write_word(COMMAND_MANUFACTURER_ACCESS, key & 0xFFFF);
    test_unsealed();
    print_operation_status();
}

void print_unseal_key() {

    i2c_smbus_write_word(COMMAND_MANUFACTURER_ACCESS, 0x60);
    uint16_t unseal_key = i2c_smbus_read_word(COMMAND_MANUFACTURER_ACCESS);

    Serial.print("Unseal key (1): 0x");
    Serial.println(unseal_key, HEX);

    uint8_t key[4];
    i2c_smbus_read_data(0x60, 4, key);

    Serial.print("Unseal key: 0x");
    for (uint8_t b : key) {
        Serial.print(b, HEX);
    }
    Serial.println();
}

void loop() {
    Serial.println("Reading data...");

    uint8_t size = i2c_smbus_read_block(COMMAND_DEVICE_CHEMISTRY, buffer);
    if (size > 5) {
        Serial.println("Can not read chemistry. Make sure all cables are plugged in correctly!");
        Serial.print("Read: ");
        Serial.write(buffer, size);
        Serial.println();
        delay(30000);
        return;
    }


    print_manufacturer_data();

    print_static_info();
    print_dynamic_data();


    print_pf_status();
    print_operation_status();


    print_unseal_key();

    uint8_t new_key[4] = {0x12, 0x34, 0x56, 0x78};
    i2c_smbus_write_data(0x60, 4, new_key);

    print_unseal_key();

    //    Serial.print("Manufacturer data: 0x");
    //    Serial.println(i2c_smbus_read_word(COMMAND_MANUFACTURER_ACCESS));

    Serial.println();
    Serial.println();
    Serial.println("All data read, doing again in 30 secs!");
    delay(30000);
}

void setup() {
    Serial.begin(115200);
    Serial.println(i2c_init());
    pinMode(SCL, INPUT_PULLUP);
    pinMode(SDA, INPUT_PULLUP);


    while (!Serial) {}

    Serial.println("Serial Initialized");

    i2c_init();
    Serial.println("I2C Initialized");
}
